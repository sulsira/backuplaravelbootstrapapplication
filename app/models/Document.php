<?php

class Document extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = ['title',
'entity_type',
'entity_ID',
'type',
'fullpath',
'filename',
'foldername',
'extension',
'filetype',
'thumnaildir',
'userID',
'deleted'];

public function landlord(){
	return $this->belongsTo('Landlord','entity_ID','id');
}
public function customer(){
	return $this->belongsTo('Customer','entity_ID','cust_id');
}
public function tenant(){
	return $this->belongsTo('Tenant','entity_ID','tent_id');
}
public function person(){
	return $this->belongsTo('Person','entity_ID','id');
}
}
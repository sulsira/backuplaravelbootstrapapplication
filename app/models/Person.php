<?php

class Person extends \Eloquent {
	protected $table = 'persons';
	protected $fillable = ['pers_fname','pers_mname','pers_lname','pers_type', 'pers_DOB', 'pers_gender','pers_nationality','pers_ethnicity','pers_NIN'];

public function customers(){
	return $this->hasOne('Customer','cust_personID');
}
public function agent(){
	return $this->hasOne('Agent','agen_persID','id');
}
public function scopeStaffs($query,$type){
	return $query->whereRaw('pers_type = ? AND id=?',['Staff',$type])->get();
}
public function contacts(){
	return $this->hasMany('Contact','Cont_EntityID');
}
public function addresses(){
	return $this->hasMany('Address','Addr_EntityID');
}
public function landlord(){
	return $this->hasOne('Landlord','ll_personid');
}
public function tenance(){
	return $this->hasOne('Tenant','tent_personid','id');
}
public function documents(){
	return $this->hasMany('Document','entity_ID');
}
public function staff(){
	return $this->hasOne('Staff','staff_personid','id');
}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('UserRoles', function(Blueprint $table)
		{
			$table->string('department')->nullable()->after('fullname');
		    $table->integer('phone')->default(0)->after('department');
		    $table->string('thumbnail')->nullable()->after('phone');
		    $table->string('image')->nullable()->after('thumbnail');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('UserRoles', function(Blueprint $table)
		{
			
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('UserRoles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')
		      ->references('id')->on('users');
		    $table->integer('sign_id')->default(0);
		    $table->string('previledges')->nullable();
		    $table->integer('security_level')->default(0);
		    $table->string('domain')->nullable();
		    $table->string('usergroup')->nullable();
		    $table->string('url')->nullable();
		    $table->string('fullname')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('UserRoles');
	}

}

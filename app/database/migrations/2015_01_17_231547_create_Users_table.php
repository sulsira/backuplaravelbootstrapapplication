<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{

			$table->increments('id')->primary();
			$table->integer('codesID')->unsigned()->index();
			$table->foreign('codesID')
		      ->references('id')->on('accessCodes');
			$table->string('username',60)->nullable();
			$table->string('email')->unique();
			$table->string('password')->nullable();
			$table->integer('status')->default(0);
			$table->softDeletes();
			$table->timestamps();
			$table->rememberToken();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

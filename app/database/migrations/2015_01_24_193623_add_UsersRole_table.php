<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUsersRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('UserRoles', function(Blueprint $table)
		{
			$table->string('filename')->nullable()->after('image');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('UserRoles', function(Blueprint $table)
		{
			$table->dropColumn('filename');
		});
	}

}

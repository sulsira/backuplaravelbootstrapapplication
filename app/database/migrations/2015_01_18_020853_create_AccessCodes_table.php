<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accessCodes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')
		      ->references('id')->on('users');

		    $table->integer('creator_id')->unsigned()->index();
			$table->foreign('creator_id')
		      ->references('id')->on('users');
		    $table->string('code', 8)->unique();
		    $table->dateTime('expired_on')->nullable();
		    $table->string('in_use')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accessCodes');
	}

}
